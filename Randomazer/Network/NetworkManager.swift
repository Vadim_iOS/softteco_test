//
//  NetworkManager.swift
//  Randomazer
//
//  Created by Vadzim Sasnouski on 3/22/20.
//  Copyright © 2020 Vadzim Sasnouski. All rights reserved.
//

import Foundation
import Alamofire

struct NetworkManager {
	static let url = AppExtension.ApiURL.url
	
	static func requestUsers(completion: @escaping (_ completed: Bool, _ error: String?, _ message: UserModel?) -> ()) {
		
		let requestUrl = url + "&results=100"
		
		AF.request(requestUrl).responseJSON { responseJSON in
			guard let statusCode = responseJSON.response?.statusCode else { return completion(false, "error", nil) }
			let messagesArray = responseJSON.value
			
			guard let json = try? JSONSerialization.data(withJSONObject: messagesArray) else { return }
			let decoder = JSONDecoder()
			guard let decodedUsers = try? decoder.decode(RecognizeResponse.self, from: json) else { return }
			
			switch statusCode {
			case 200:
				completion(true, nil, decodedUsers.messages)
			default:
				let errorBody = "Server error, statusCode: \(statusCode)"
				completion(false, errorBody, nil)
			}
		}
	}
}
