//
//  Response.swift
//  Randomazer
//
//  Created by Vadzim Sasnouski on 3/22/20.
//  Copyright © 2020 Vadzim Sasnouski. All rights reserved.
//

import Foundation

struct RecognizeResponse {
    let messages: UserModel
}

extension RecognizeResponse: Codable {
    init(from decoder: Decoder) throws {
        self.init(messages: try UserModel(from: decoder))
    }
    
    func encode(to encoder: Encoder) throws {
        try messages.encode(to: encoder)
    }
}
