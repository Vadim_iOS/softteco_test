//
//  UserModel.swift
//  Randomazer
//
//  Created by Vadzim Sasnouski on 3/22/20.
//  Copyright © 2020 Vadzim Sasnouski. All rights reserved.
//

import Foundation

struct UserModel: Codable {
	let results: [UserData]
	let info: Info
}

struct Info: Codable {
	let seed: String
	let results, page: Int
	let version: String
}

struct UserData: Codable {
	let picture: Picture
	let name: Name
	let location: Location
	let email: String
	let phone: String
	let id: ID
}

struct Picture: Codable {
	let large, medium, thumbnail: String
}

struct Name: Codable {
	let title, first, last: String
}

struct Location: Codable {
	let country, city: String
}

struct ID: Codable {
	let name: String
	let value: String?
}
