//
//  UIButtonExtension.swift
//  Randomazer
//
//  Created by Vadzim Sasnouski on 3/22/20.
//  Copyright © 2020 Vadzim Sasnouski. All rights reserved.
//

import UIKit

extension UIButton {
	func setupButton() {
		self.layer.cornerRadius = self.bounds.height / 4
		
		self.setTitleColor(.white, for: .normal)
		self.backgroundColor = AppExtension.AppColors.buttons
		
		self.clipsToBounds = true
	}
}
