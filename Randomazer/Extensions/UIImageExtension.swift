//
//  UIImageExtension.swift
//  Randomazer
//
//  Created by Vadzim Sasnouski on 3/22/20.
//  Copyright © 2020 Vadzim Sasnouski. All rights reserved.
//

import UIKit

extension UIImageView {
    func roundImage() {
        self.layer.cornerRadius = self.bounds.height / 2
        self.clipsToBounds = true
    }
}
