//
//  App.extension.swift
//  Randomazer
//
//  Created by Vadzim Sasnouski on 3/22/20.
//  Copyright © 2020 Vadzim Sasnouski. All rights reserved.
//

import Foundation
import UIKit

struct AppExtension {
	struct AppColors {
		static let buttons = UIColor(red: 89.0 / 255.0, green: 130.0 / 255.0, blue: 174.0 / 255.0, alpha: 1.0)
	}
	
	struct ApiURL {
		static let url = "https://randomuser.me/api/?inc=picture,name,location,email,phone,id"
	}
}
