//
//  ViewController.swift
//  Randomazer
//
//  Created by Vadzim Sasnouski on 3/22/20.
//  Copyright © 2020 Vadzim Sasnouski. All rights reserved.
//

import UIKit
import SDWebImage

class TableViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	
	var array: [UserData] = []

	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.dataSource = self
		tableView.delegate = self
		setupUI()
		setupTableData()
	}
	
	func setupUI() {
		tableView.rowHeight = 70.0
		tableView.isHidden = true
		
		activityIndicator.startAnimating()
	}

	func setupTableData() {
		NetworkManager.requestUsers { (completed, error, responseMessage) in
			if completed {
				responseMessage?.results.forEach { self.array.append($0) }
				self.activityIndicator.stopAnimating()
				self.tableView.isHidden = false
				self.tableView.reloadData()

			} else {
				let alert = UIAlertController(title: "Warning", message: "Service not reachable. Please try again later", preferredStyle: .alert)
				alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
					self.navigationController?.popToRootViewController(animated: true)
				}))
				self.present(alert, animated: true, completion: nil)
			}
		}
	}
	
	func displayImages(imageURL: String, imageView: UIImageView) {
		guard let url = URL(string: imageURL) else { return }
		DispatchQueue.main.async {
			imageView.sd_setImage(with: url, completed: nil)
            
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.0, options: [], animations: {
                imageView.center.y += 60.0
            }, completion: nil)
		}
	}
}

extension TableViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		array.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as! TableViewCell
		
		cell.userNameLabel.text = (array[indexPath.row].name.first + " " + array[indexPath.row].name.last)
		cell.userAddresslabel.text = (array[indexPath.row].location.city + ", " + array[indexPath.row].location.country)
		cell.userPhoneNumberLabel.text = array[indexPath.row].phone
		displayImages(imageURL: array[indexPath.row].picture.medium, imageView: cell.avatarImageView)
		
		cell.selectionStyle = .none
		return cell
	}
}

extension TableViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let detailVC = storyboard?.instantiateViewController(withIdentifier: "userDetails") as! DetailsViewController

		detailVC.userDataInfo = array[indexPath.row]
		
		self.navigationController?.pushViewController(detailVC, animated: true)
	}
}

