//
//  DetailsViewController.swift
//  Randomazer
//
//  Created by Vadzim Sasnouski on 3/22/20.
//  Copyright © 2020 Vadzim Sasnouski. All rights reserved.
//

import UIKit
import SDWebImage
import MessageUI

class DetailsViewController: UIViewController, MFMailComposeViewControllerDelegate {

	@IBOutlet weak var detailUserAvatarImageView: UIImageView!
	
	@IBOutlet weak var detailUserNameLabel: UILabel!
	
	@IBOutlet weak var streetLabel: UILabel!
	@IBOutlet weak var cityLabel: UILabel!
	@IBOutlet weak var countryLabel: UILabel!
	
	@IBOutlet weak var sendEmailButton: UIButton!
	@IBOutlet weak var callFirstPhoneButton: UIButton!
	@IBOutlet weak var callSecondPhoneButton: UIButton!
	
	@IBOutlet weak var otherDetailsLabel: UILabel!
	
	var userDataInfo: UserData?
	
	override func viewDidLoad() {
        super.viewDidLoad()

		setupUI()
    }
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		detailUserAvatarImageView.roundImage()
	}
    
	func setupUI() {
		guard let userData = userDataInfo else { return }
		
		DispatchQueue.main.async {
			guard let url = URL(string: userData.picture.large) else { return }
			self.detailUserAvatarImageView.sd_setImage(with: url, completed: nil)
		}
		
		detailUserNameLabel.text = (userData.name.first + " " + userData.name.last)
		
		streetLabel.isHidden = true
		cityLabel.text = userData.location.city
		countryLabel.text = userData.location.country
		
		sendEmailButton.setupButton()
		let mailButtonTitle = "Send email to \(userData.email)"
		sendEmailButton.setTitle(mailButtonTitle, for: .normal)
		callFirstPhoneButton.setupButton()
		let phoneNumber = "Call \(userData.phone)"
		callFirstPhoneButton.setTitle(phoneNumber, for: .normal)
		callSecondPhoneButton.isHidden = true
		
		otherDetailsLabel.text = "Other details"
	}

	@IBAction func emailButtonDidPress(_ sender: UIButton) {
		guard let userData = userDataInfo else { return }
		if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
			mail.setToRecipients([userData.email])
            mail.setMessageBody("<p>Hello, bro!</p>", isHTML: true)

            present(mail, animated: true)
        }
	}
	
	@IBAction func firstPhoneDidPress(_ sender: UIButton) {
		guard let userData = userDataInfo else { return }
		guard let number = URL(string: "tel://" + userData.phone) else { return }
		UIApplication.shared.open(number)
	}
	
	@IBAction func secondPhoneDidPress(_ sender: UIButton) {
	}
	
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
