//
//  CollectionViewCell.swift
//  Randomazer
//
//  Created by user on 23/03/2020.
//  Copyright © 2020 Vadzim Sasnouski. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userAddress: UILabel!
    @IBOutlet weak var userPhone: UILabel!
}
