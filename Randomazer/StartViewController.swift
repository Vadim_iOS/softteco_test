//
//  StartViewController.swift
//  Randomazer
//
//  Created by Vadzim Sasnouski on 3/22/20.
//  Copyright © 2020 Vadzim Sasnouski. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

	@IBOutlet weak var tableOpenButton: UIButton!
	@IBOutlet weak var collectionOpenButton: UIButton!
    @IBOutlet weak var stackViewCenter: NSLayoutConstraint!
    
	let tableOpenButtonName = "TABLE"
	let collectionOpenButtonName = "COLLECTION"
	
	override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        stackViewCenter.constant = 0
        
        UIView.animate(withDuration: 1) { [weak self] in
            self?.view.layoutSubviews()
        }
    }
    
	func setupUI() {
		tableOpenButton.setupButton()
		tableOpenButton.setTitle(tableOpenButtonName, for: .normal)
		
		collectionOpenButton.setupButton()
		collectionOpenButton.setTitle(collectionOpenButtonName, for: .normal)
        
        stackViewCenter.constant = -view.bounds.width
	}

}
