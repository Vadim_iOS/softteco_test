//
//  CollectionViewController.swift
//  Randomazer
//
//  Created by user on 23/03/2020.
//  Copyright © 2020 Vadzim Sasnouski. All rights reserved.
//

import UIKit

class CollectionViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionActivityIndicator: UIActivityIndicatorView!
    
    var array: [UserData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        
        let nib = UINib(nibName: "CollectionCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "collectionCell")
        
        collectionActivityIndicator.startAnimating()
        collectionView.isHidden = true
        setupTableData()
    }
    
    func setupTableData() {
        NetworkManager.requestUsers { (completed, error, responseMessage) in
            if completed {
                responseMessage?.results.forEach { self.array.append($0) }
                self.collectionActivityIndicator.stopAnimating()
                self.collectionView.isHidden = false
                self.collectionView.reloadData()

            } else {
                let alert = UIAlertController(title: "Warning", message: "Service not reachable. Please try again later", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    self.navigationController?.popToRootViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func displayImages(imageURL: String, imageView: UIImageView) {
        guard let url = URL(string: imageURL) else { return }
        DispatchQueue.main.async {
            imageView.sd_setImage(with: url, completed: nil)
        }
    }
}

extension CollectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! CollectionViewCell
        
        cell.userName.text = (array[indexPath.row].name.first + " " + array[indexPath.row].name.last)
        cell.userAddress.text = (array[indexPath.row].location.city + ", " + array[indexPath.row].location.country)
        cell.userPhone.text = array[indexPath.row].phone
        displayImages(imageURL: array[indexPath.row].picture.medium, imageView: cell.userAvatar)
        
        return cell
    }
}

extension CollectionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailVC = storyboard?.instantiateViewController(withIdentifier: "userDetails") as! DetailsViewController

        detailVC.userDataInfo = array[indexPath.row]
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 2

        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

        return CGSize(width: size, height: size)
    }
}
